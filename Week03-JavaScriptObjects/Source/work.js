#!/usr/bin/env node

/**
 * Created by Jefferson Eagley on 4/24/16.
 */

var person = {
    firstName: 'George',
    lastName: 'Washington',
    fullName: function() {
        'use strict';
        return this.firstName + ' ' + this.lastName;
    }
};

var calculator = {
    operator01: -1,
    operator02: -1,
    add: function() {
        'use strict';
        return this.operator01 + this.operator02;
    },
    subtract: function() {
        'use strict';
        return this.operator01 - this.operator02;
    },
    multiply: function() {
        'use strict';
        return this.operator01 * this.operator02;
    },
};

function divider(title) {
    'use strict';
    console.log('====================================');
    console.log(title);
    console.log('====================================');
}

function objectPrintout(obj) {
    'use strict';
    for (var v in obj) {
        if (typeof obj[v] === 'function') {
            console.log(v, ': ', obj[v]());
        } else {
            console.log(v, ': ', obj[v]);
        }
    }

}

calculator.operator01 = person.firstName.length;
calculator.operator02 = person.lastName.length;

divider('Person');
console.log(person.firstName);

console.log(person.lastName);

console.log(person.fullName());

divider('Calculator');

objectPrintout(calculator);

module.exports = person;
module.exports.push(divider);
