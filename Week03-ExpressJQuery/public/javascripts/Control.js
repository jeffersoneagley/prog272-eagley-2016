/**
 * Created by Jefferson on 4/19/16.
 */

var MyObject = (function () {

    function MyObject() {
        $('#sendString').click(showString);
        $('#getItems').click(showItems);
        $('#getMarie').click(showMarie);
    }

    function showString() {
        $('#stringHolder').html('Send string was clicked');
    }

    function showItems () {
        $("#myList").append('<li>'+'item01'+'</li>');
        $("#myList").append('<li>'+'item02'+'</li>');
        $("#myList").append('<li>'+'item03'+'</li>');
    }

    function showMarie() {
        $.getJSON('marie.json', function(marie) {
            // The variable marie now holds your JSON. Process it and display it as shown above.
            for(var property in marie){
                if(marie.hasOwnProperty(property)){
                    $("#myList").append('<li>'+marie[property]+'</li>');
                }
            }
        });


    }

    MyObject.prototype.readyCalled = function () {
        $('#readyCalled').html('Ready was called and myObject created');
    };
    return MyObject;
}());

$(document).ready(function () {
    console.log("Control.js loaded");
    var myObject = new MyObject();
    myObject.readyCalled();
});
