var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Week04 Express Routes' });
});

router.get('/read', function (request, response) {
    console.log("Ser: read was called on the server");
    response.send([
        {"name":"Nyannyan"},
        {"name": "Fish"}
    ]);
});

router.get('/readDerp', function (request, response) {
    console.log("Ser: read derp was called on the server");
    response.send("<em>Derp</em>");
});

router.get('/add', function (req, res) {
    var response = {
        a:req.query.a,
        b:req.query.b
    }
    response.result = parseFloat(req.query.a)+ parseFloat(req.query.b);
    console.log("serv: add was called,"+response.a+"+"+response.b+"="+response.result);

    res.send(
        response
    )
});


router.get('/:id', function (req, res) {
    res.send("you navigated to /"+req.params.id
        +", was that a mistake?</br> here's a return link: "
        +"<a href='/'>main page</a>");
});


module.exports = router;
