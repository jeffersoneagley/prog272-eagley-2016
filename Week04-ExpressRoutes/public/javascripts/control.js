/**
 * Created by bcuser on 4/25/16.
 */
$(document).ready(function() {
    console.log('Document loaded in prog272');

    $('#read').click(callRead);
    $('#readJSON').click(callReadJSON);
    $('#readDerp').click(callReadDerp);
    $('#add').click(add);

    function callRead() {
        console.log('callRead called');
        $.getJSON('/read', function (result){
            console.log(result);
            $('#display').html(JSON.stringify(result));
        })
    }


    function callReadJSON() {
        console.log('callReadJSON called');
        $.getJSON('names.json', function (result){
            console.log(result);
            $('#display').html(JSON.stringify(result));
        })
    }
    function callReadDerp() {
        console.log('callReadDerp called');
        $.get('/readDerp', function (result){
            console.log(result);
            $('#display').html(result.toString());
        })
    }

    function add(){

        var a = $('#adda').val();
        var b = $('#addb').val();
        var requestQuery = {
            a:$('#adda').val(),
            b:$('#addb').val()
        }
        $.getJSON('/add', requestQuery, function (response) {

            console.log(response);
            $('#addout').html(response.a+"+"+response.b+"="+response.result);

        })
    }
});