var myObject = {
    fish: 'fish',
    x: 0,
    y: 0,
    getDerp: function() {
        'use strict';
        return 'derp';
    },
    getNyan: function() {
        'use strict';
        return 'nyan';
    },
    getGonk: function() {
        'use strict';
        return 'gonk';
    }
};
module.exports = myObject;
