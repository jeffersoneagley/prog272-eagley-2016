$(document).ready(function() {
    'use strict';
    $('#buttonAllRenewables').click(getRenewables);
    $('#buttonRenewableByYear').click(getRenewableByYear);
    $('#buttonRenewableByIndex').click(getRenewableByIndex);
});

function getRenewables() {
    'use strict';
    getRenewablesStringInput('/renewables');
}

function getRenewableByYear() {
    'use strict';
    var year = parseInt($('#inputYear').val());
    if (year !== undefined && year >= 2000) {
        getRenewablesStringInput('renewablesByYear/' + year);
    } else {
        console.log('invalid year ' + year);
        $('#debug').html('Please enter a valid year.');
    }
}

function getRenewableByIndex() {
    'use strict';
    var index = parseInt($('#inputIndex').val());
    if (index !== undefined && index >= 0) {
        getRenewablesStringInput('renewablesByIndex/' + index);
    } else {
        console.log('invalid index ' + index);
        $('#debug').html('Please enter a valid index.');
    }

}

function getRenewablesStringInput(sourceString) {
    'use strict';
    $.getJSON(sourceString, function(data) {
            var outputBox = $('#output');
            if (data.renewables === undefined) {
                outputBox.html('No data for the requested location');
            } else {
                outputBox.text(JSON.stringify(data.renewables, null, 4));
            }
            console.log(data);
        })
        .done('Succeeded in getting data')
        .fail(function(a, b, c) {
            console.log('error', a, b, c);
            $('#debug').html('Error occured: ' + a.status);
        })
        .always('Command completed');

}
