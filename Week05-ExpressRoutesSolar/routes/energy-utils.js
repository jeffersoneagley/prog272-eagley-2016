/** Created by Jefferson on 5/11/2016 */

function objectToArray(obj) {
    'use strict';
    console.log('begin objectToArray');
    var objectAsArray = [];
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            objectAsArray.push([key, obj[key]]);
        }
    }
    console.log('objectAsArray parsed values:\n' + objectAsArray);

    objectAsArray.sort(function(a, b) {
        return a[1] > b[1];
    });
    console.log('objectToArray function complete, returning');
    return objectAsArray;
}

module.exports.objectToArray = objectToArray;
