var express = require('express');
var router = express.Router();

var fs = require('fs');
//var objectToArray = require('objectToArray');
/* GET home page. */

router.get('/renewablesByIndex/:id', function(req, res) {
    'use strict';
    var id = parseInt(req.params.id);
    if (id < 0) {

        res.status(500).send('That\'s not a valid index', 'please enter a valid index');
    }
    console.log('renewablesByIndex called ', req.params.id);
    fs.readFile('data/Renewable.json', 'utf8', function(err, data) {
        if (err) {
            res.status(404).send(err);
        } else {
            //console.log(data);
            var json = JSON.parse(data);
            res.send({
                result: 'success',
                renewables: json[
                    id
                ]
            });

        }
    });
});
router.get('/renewablesByYear/:id', function(req, res) {
    'use strict';
    console.log('renewablesByYear called', req.params.id);
    fs.readFile('data/Renewable.json', 'utf8', function(err, data) {
        if (err) {
            res.status(404).send(err);
        } else {
            //console.log(data);
            var json = JSON.parse(data);
            for (var i = 0; i < json.length; i++) {
                if (json[i].Year == req.params.id) {
                    res.send({
                        result: 'success',
                        renewables: json[i]
                    });
                }
            }
        }
    });
});

router.get('/renewablesSorted', function(req, res) {
    'use strict';
    console.log('renewablesSorted called');
});

router.get('/renewables', function(req, res) {
    'use strict';
    console.log('Renewables called');
    fs.readFile('data/Renewable.json', 'utf8', function(err, data) {
        if (err) {
            res.status(404).send(err);
        } else {
            //console.log(data);
            res.send({
                result: 'success',
                renewables: JSON.parse(data)
            });
        }
    });
});

router.get('/', function(req, res, next) {
    'use strict';
    res.render('index', {
        title: 'Alternative Energy Database'
    });
});

module.exports = router;
