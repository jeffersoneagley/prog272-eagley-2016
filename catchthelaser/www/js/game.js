﻿
var divContainer = document.querySelector("#jeCatGame");

var canvas = document.createElement("canvas");
var ctx = canvas.getContext("2d");

canvas.style.height = "auto";
canvas.style.width = "100%";

canvas.style.maxWidth = "100%";
canvas.style.maxHeight = "100%";
canvas.style.margin = "1em auto;";
canvas.style.cursor = "crosshair";
canvas.style.cursor = "url('app/img/pointer.png') 15 15, url('app/img/pointer.cur') 15 15, crosshair";

canvas.width = 640;
canvas.height = 480;
var image = new Image();
image.src = "app/img/Empty%20Floor.png";
image.addEventListener("load", function () {
    game.screen.start();
}, false);


divContainer.appendChild(canvas);
divContainer.style.height = "90%";

var canvasClick = function () {
    game.playing = true;
    init();
};

var game = {
    playing: false,
    score: 0,
    screen: {
        start: function () {
            game.playing = false;
            game.setStripeFill(ctx);
            ctx.textAlign = "center";
            game.stripeText(ctx, "Get the Laser!!!", Math.round(canvas.width / 2), 55, 3, "50px 'arial black'");
            game.stripeText(ctx, "You are a sleeping cat. You awaken from your nap to", Math.round(canvas.width / 2), 80, 2, "21px 'arial black'");
            game.stripeText(ctx, "a swarm of multicolored dots flying around the room.", Math.round(canvas.width / 2), 100, 2, "21px 'arial black'");
            game.stripeText(ctx, "To a cat, this can only mean one thing...", Math.round(canvas.width / 2), 120, 2, "21px 'arial black'");
            game.stripeText(ctx, "Catch all the dots!!!", Math.round(canvas.width / 2), 140, 2, "21px 'arial black'");


            var start = {
                x: canvas.width / 2,
                y: canvas.height - 20,
                width: 200,
                height: 50,
                refresh: function (ctx) {
                    ctx.lineWidth = 4;
                    ctx.fillRect(this.x - (this.width / 2), this.y - (this.height / 2), this.width, this.height);
                    ctx.strokeRect(this.x - (this.width / 2), this.y - (this.height / 2), this.width, this.height);
                    ctx.textAlign = "center";
                    game.stripeText(ctx, "Start!", this.x, this.y, 2, "25px 'arial black'");
                },
                clickHandler: function (x, y) {
                    if (x > this.x - (this.width / 2) && x < this.x - (this.width / 2) + (this.width * 1.1) &&
                        y > this.y - (this.height / 2) && y < this.y - (this.height / 2) + (this.height * 1.1)) {
                        canvasClick;
                    }
                }

            }
            start.refresh(ctx);
            canvas.addEventListener("click", canvasClick, false);
        },
        end: function () {
            animItems = [];
            game.playing = false;
            var sleepy = new sprite("app/img/sleepy.png", "sleepy");
            sleepy.x = canvas.width / 2;
            sleepy.y = canvas.height / 2;

            sleepy.AVAILABLESTATES.idle.width = 32;
            sleepy.AVAILABLESTATES.idle.height = 32;
            sleepy.AVAILABLESTATES.idle.x = 0;
            sleepy.AVAILABLESTATES.idle.y = 0;
            sleepy.AVAILABLESTATES.idle.frameEnd = 3;
            sleepy.AVAILABLESTATES.idle.keyFrame = 20;

            sleepy.visible = true;
            var txt = {
                refresh: function (ctx) {
                    ctx.textAlign = "center";
                    game.stripeText(ctx, "Game Over", Math.round(canvas.width/2), 55, 3, "50px 'arial black'");
                    game.stripeText(ctx, "The sleepy cat is out of energy! Time for a nap.", Math.round(canvas.width / 2), 80, 2, "21px 'arial black'");
                    var txt;
                    if (game.score < 1) {
                        txt = "The evil dots escaped you again!";
                    } else if (game.score < 5) {
                        txt = "You caught only " + game.score + " evil dots! Better hunt again soon.";
                    } else if (game.score < 9) {
                        txt = "You caught " + game.score + " evil dots!";
                    } else {
                        txt = "Amazing!!! You caught " + game.score + " evil dots!!! Killer Kitty!";
                    }
                    game.stripeText(ctx, txt, Math.round(canvas.width / 2), 105, 2, "21px 'arial black'");
                }
            }

            var playAgain = {
                x: canvas.width / 2,
                y: canvas.height - 20,
                width: 200,
                height: 50,
                refresh: function(ctx){
                    ctx.lineWidth = 4;
                    ctx.fillRect(this.x - (this.width / 2), this.y - (this.height / 2), this.width, this.height);
                    ctx.strokeRect(this.x - (this.width / 2), this.y - (this.height / 2), this.width, this.height);
                    ctx.textAlign = "center";
                    game.stripeText(ctx, "Play again!", this.x, this.y, 2, "25px 'arial black'");
                },
                clickHandler: function (x, y) {
                    if (x > this.x - (this.width / 2) && x < this.x - (this.width / 2) + (this.width * 1.1) &&
                        y > this.y - (this.height / 2) && y < this.y - (this.height / 2) + (this.height * 1.1)) {
                        location.reload();
                    }
                }

            }
                    animItems.push(sleepy);
                    animItems.push(txt);
                    animItems.push(playAgain);

        }
    },
    setStripeFill: function (ctx) {
        ctx.strokeStyle = "gold";
        ctx.fillStyle = ctx.createPattern(image, "repeat");
    },
    stripeText: function (ctx, txt, x, y, level, font) {
        level = level || 3;
        font = font || "20px, 'arial black'";
        ctx.font = font
        ctx.fillText(txt, x, y);
        ctx.lineJoin = "round";
        ctx.strokeStyle = "#CCAD00";
        ctx.lineWidth = level;
        ctx.strokeText(txt, x, y);

        ctx.strokeStyle = "gold";
        ctx.lineWidth = level-1;
        ctx.strokeText(txt, x, y);

        ctx.strokeStyle = "#CCAD00";
        ctx.lineWidth = 1;
        ctx.strokeText(txt, x, y);
        ctx.restore();
    },
    drawScore: function (ctx) {
        ctx.save();
        this.setStripeFill(ctx);
        ctx.font = "50px 'Arial black'";
        ctx.textAlign = "right";
        var txt = "Kills: " + this.score;
        var x = ctx.canvas.width;
        var y = (ctx.canvas.height - 30);

        //create outline
        this.stripeText(ctx, txt, x, y,  2);
    },
    drawPlayerStats: function (ctx, playerArray) {
        var image = new Image();
        image.src = "app/img/Empty%20Floor.png";

        ctx.save();
        ctx.globalAlpha = 0.75;

        ctx.fillStyle = "gold";

        var SIZE = 20;
        var WIDTH = 150;
        var PADDING = 5;
        var font = (SIZE - 2) + "px 'Arial'";
        for (var p = 0; p < playerArray.length; p++) {
            var txt = playerArray[p].name || ("player "+p);
            var x = 20;
            var y = SIZE * 2 * (p + 1);
            ctx.textAlign = "left";
            game.stripeText(ctx, txt, x, y, 2, font);
            y += PADDING;
            ctx.save();
            ctx.strokeStyle = "rgba(46, 139, 87, 0.50)"; //seagreen
            ctx.fillStyle = "rgba(102, 205, 170, 0.50)"; //mediumaquamarine
            ctx.strokeRect(x - PADDING, y, WIDTH, SIZE+PADDING);
            ctx.fillRect(x - PADDING, y, Math.round(WIDTH * playerArray[p].energy / playerArray[p].energyMax), SIZE + PADDING);
            ctx.strokeRect(x - PADDING, y, Math.round(WIDTH * playerArray[p].energy / playerArray[p].energyMax), SIZE + PADDING);
            y += SIZE - 2;
            ctx.restore();
            txt = "Energy: " + playerArray[p].energy;
            //draw energy text
            ctx.textAlign = "left";
            game.stripeText(ctx, txt, x, y, 2, font);
            y += PADDING;

        }
            ctx.restore();
    },
    checkWin: function () {
        if (playerArray[0].energy < 1) {
            game.screen.end();
        }
    }
}

var playerArray = [
    ply = {
        name: "Kitty",
        energy: 100,
        energyMax: 100
    }
]


//var catMitten = sprite("/app/img/catmitten.png", "Mitten");
//var catJoey = sprite("/app/img/catjoey.png", "Joey");
//var catFish = sprite("/app/img/catfish.png", "Fish");

var init = function () {
    canvas.removeEventListener("click", canvasClick);
    var paw = makeCatPaw("app/img/catmitten.png", "mitten");
    paw.img.addEventListener("load", loadHandler, false);
    console.log(paw);
    animItems.push(paw);
    paw.x = canvas.width - paw.width;
    paw.y = canvas.height - paw.height;
    paw.clickHandler = function (e) {
        playerArray[0].energy--;
        game.checkWin();
        paw.attack(e.x, e.y);
    }
    animItems.push(makeLaserDot())
    animItems[1].visible = true;
    animItems.push(makeLaserDot())
    animItems[2].color.r = 255;
    animItems[2].color.g = 100;
    animItems[2].color.b = 100;

    animItems.push(makeLaserDot())
    animItems[3].color.b = 255;
    animItems[3].color.g = 200;
    animItems[3].color.r = 100;
    //add paw location update tied to mouse
    mouse.refresh = function () {
        paw.x = mouse.x;
        paw.y = mouse.y;
    }
}
var animItems = [];



mouse.init(canvas);

//objTimer.start();

function refresh() {
    
    //visibility refresh
    for (var i = 0; i < animItems.length; i++) {
        if (animItems[i].updatePhysics != undefined) {
            animItems[i].updatePhysics(ctx);
        }
        animItems[i].refresh(ctx);
    }
    if (game.playing) {
        game.drawScore(ctx);
        game.drawPlayerStats(ctx, playerArray)
    }
}

canvas.addEventListener("click", function (e) {
    var loc = mouse.getPixelFromScreen(canvas, e);
    for (var i = 0; i < animItems.length; i++) {
        if (animItems[i].clickHandler != undefined) {
            animItems[i].clickHandler(loc.x, loc.y);
        }
    }
}, "false")


function animLoop() {
    requestAnimationFrame(animLoop, canvas);
    // Clear context
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    ctx.save();
    ctx.fillStyle = "RGBA(255,255,255,0.10)";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.restore();

    refresh();
}

function loadHandler() {
    animLoop();

}

