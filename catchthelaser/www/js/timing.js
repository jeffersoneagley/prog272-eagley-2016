﻿// 
// timer function
//

var objTimer =
{
    time: 0,
    intervalTimer: undefined,
    milliseconds:0,
    start: function () {
        var self = this;
        this.intervalTimer = setInterval(function () { self.tick(); }, 100);
    },
    tick: function () {
        this.milliseconds += 100;
        if (this.milliseconds >= 1000) {
            this.time--;
            this.milliseconds -= 1000;
        }
        if (this.time < 1) {
            this.stop();
        }
    },
    stop: function () {
        clearInterval(this.intervalTimer);
    },
    reset: function () {
        this.time = 0;
    }
};