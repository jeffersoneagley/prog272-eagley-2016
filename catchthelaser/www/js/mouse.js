﻿var mouse = {
    x: 0,
    y: 0,
    refresh: function () { },
    getMousePos: function(canvas, outputObject, evt){
        var rect = canvas.getBoundingClientRect(), root = document.documentElement;
        outputObject.x = (evt.clientX - rect.left - root.scrollLeft) * (canvas.width / canvas.clientWidth);
        outputObject.y = (evt.clientY - rect.top - root.scrollTop) * (canvas.height / canvas.clientHeight);
        outputObject.refresh();
    },
    getPixelFromScreen: function (canvas, evt) {

        var rect = canvas.getBoundingClientRect(), root = document.documentElement;
        var outputObject = {};
        outputObject.x = Math.round((evt.clientX - rect.left - root.scrollLeft) * (canvas.width / canvas.clientWidth));
        outputObject.y = Math.round((evt.clientY - rect.top - root.scrollTop) * (canvas.height / canvas.clientHeight));
        //console.log(evt.x, event.y, outputObject);
        return outputObject;
    },
    init: function (canvas) {
        var self = this;
        canvas.addEventListener("mousemove", function (e) { self.getMousePos(canvas, self, e);}, false);
    }
}

