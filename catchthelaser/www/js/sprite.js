﻿var ANIM = {
    switchFrameEndpoints: function(self){
        self.frameEnd = self.frameStart;
        self.frameStart = self.frame;
    },
    STATE: {
        TEMPLATE: function () {
            var self = this;
            var tmp = {
                x: 0, y: 6, width: 32, height: 32, frame: 0, frameStart: 0, frameEnd: 1, direction: true, keyFrame: 15, tick: 0, complete: false,
                animFunc:function(){
                    
                    ANIM.STATE.RUN.REWIND(this);
                },
                next: function () {
                    if (this.tick >= this.keyFrame) {
                        this.tick = 0;
                        this.animFunc();
                       // console.log("tick",this.x, this.y, this);
                    }
                    this.tick++;
                }
            };
            return tmp;
        },
        REPEAT: {
            RESET: function (self) {
                switch (self.frameStart < self.frameEnd) {
                    case true:
                        self.frame++;
                        if (self.frame >= self.frameEnd) {
                            self.frame = self.frameStart;
                        }
                        break;
                    case false:
                        self.frame--;
                        if (self.frame <= self.frameEnd) {
                            self.frame = self.frameStart;
                        }
                        break;
                }
            },
            REWIND: function (self) {
                switch (self.direction) {
                    case true:
                        self.frame++;
                        if (self.frame >= self.frameEnd) {
                            //self.frame is at self.frameEnd
                            ANIM.switchFrameEndpoints(self);
                        }
                        break;
                    case false:
                        self.frame--;
                        if (self.frame <= self.frameEnd) {
                            //self.frame is at self.frameStart
                            ANIM.switchFrameEndpoints(self);
                        }
                        break;
                }
            },
        },
        RUN: {
            REWIND: function (self) {
                //console.log("run.rewind frame ", self.frame, self.direction );
                switch (self.direction) {
                    case true:
                        self.frame++;
                        if (self.frame >= self.frameEnd) {
                            //self.frame is at self.frameEnd
                            self.direction = false;
                        }
                        break;
                    case false:
                        if (self.frame <= self.frameStart) {
                            self.frame = self.frameStart;
                            //self.frame is at self.frameStart
                            self.complete = true;
                            self.direction = true;
                            self.state = "idle";
                        } else {
                            self.frame--;
                        }
                        break;
                }
            }
        }

    }
}

function sprite(spriteSheetUrl, name) {
    var spr = {};
    spr.img = new Image();
    spr.img.src = spriteSheetUrl || "app/img/spritesheet.png";
    spr.name = name | undefined;
    spr.x = 0;
    spr.y = 0;
    spr.width= 128;
    spr.height= 128;
    spr.offsetx =  -Math.round(spr.width / 2);
    spr.offsety = -Math.round(spr.height / 2);
    spr.visible = false;
    spr.AVAILABLESTATES = {
        idle: new ANIM.STATE.TEMPLATE()
    };
    
    spr.state = "idle";
    spr.pickImage = function () {
        var curstate=this.AVAILABLESTATES[this.state];
        if (curstate.complete) {
            curstate.complete = false;
            //curstate.frame = 0;
            curstate.tick = 0;
            this.state = "idle";
        }
        curstate.next();
    };
    spr.refresh = function (context) {
        this.pickImage();
        var xloc = this.AVAILABLESTATES[this.state].x;
        var yloc = this.AVAILABLESTATES[this.state].y;
        var sourcex = xloc + (this.AVAILABLESTATES[this.state].width * this.AVAILABLESTATES[this.state].frame);
        var sourcey = yloc;
        var sourcew = this.AVAILABLESTATES[this.state].width;
        var sourceh= this.AVAILABLESTATES[this.state].height
        var xpos = (this.x + this.offsetx);
        var ypos = (this.y + this.offsety);
        context.drawImage(
            this.img,
            sourcex, sourcey, sourcew, sourceh,
            xpos, ypos, this.width, this.height
            );
    };
    return spr;
}

function makeCatPaw(spriteSheetUrl, name) {
    var paw = sprite(spriteSheetUrl, name);
    paw.width = 128;
    paw.height = 128;
    paw.offsetx = -Math.round(paw.width / 2);
    paw.offsety = 0;



    paw.state = "idle1";
    paw.AVAILABLESTATES.idle1 = new ANIM.STATE.TEMPLATE();
    paw.AVAILABLESTATES.idle1.y = 5;
    paw.AVAILABLESTATES.idle1.frameEnd = 7;

    paw.state = "idle2";
    paw.AVAILABLESTATES.idle2 = new ANIM.STATE.TEMPLATE();
    paw.AVAILABLESTATES.idle2.frameEnd = 4;
    paw.AVAILABLESTATES.idle2.y = 45;

    paw.AVAILABLESTATES.idle = new ANIM.STATE.TEMPLATE();
    paw.AVAILABLESTATES.idle.chooseState = function () {
        //console.log("paw.availablestates.idle.chooseState()");
        var idleStates = ["idle1", "idle2"];
        var stateSelected = idleStates[Math.floor(Math.random() * idleStates.length)];
        //console.log(stateSelected);
        paw.state = stateSelected;
    };
    paw.AVAILABLESTATES.idle.next = function () {
        //console.log("idle.next");
            this.chooseState();
    };

    paw.AVAILABLESTATES.down = new ANIM.STATE.TEMPLATE();
    paw.AVAILABLESTATES.down.frameEnd=4;
    paw.AVAILABLESTATES.down.y = 85;
    paw.AVAILABLESTATES.down.keyFrame = 3;

    paw.attack = function(x, y) {
        this.state = "down";
        //console.log("down");
    };

    return paw;
}
    

function makeLaserDot() {
    var dot = {
        x: 0,
        y: 0,
        vx: 0,
        vy:0,
        color: {
            r: 0,
            g: 255,
            b: 0
        },
        width: 20,
        height: 16,
        visible: true,
        randomize: true
    };
    dot.hide = function () {
        this.visible = false;
        var self = this;
        setTimeout(function () { self.visible = true }, Math.round(1222*Math.random()));
    }
    dot.randomDirection = function (ctx) {
        this.vx += Math.round(Math.random() * 2) - 1;
        this.vy += Math.round(Math.random() * 2) - 1;
        if (-10 > this.vx || this.vx > 10) {
            this.y = Math.round(ctx.canvas.height * Math.random());
            this.x = Math.round(ctx.canvas.width * Math.random());
            this.vx = 0;
            this.hide();
        }
        if (-9 > this.vy || this.vy > 9) {
            this.y = Math.round(ctx.canvas.height * Math.random());
            this.x = Math.round(ctx.canvas.width * Math.random());
            this.vy = 0;
            this.hide();
        }
        if ((this.vx * this.vx + this.vy * this.vy) > 90) {
            this.vx = Math.round(this.vx * 0.9);
            this.vy = Math.round(this.vy * 0.88);
        }
    }
    dot.clickHandler = function (x, y) {
        //console.log(x, this.x, y, this.y);
        // Check if mouse was clicked on the sprite
        if (x > this.x && x < this.x + (this.width*1.1) &&
             y > this.y && y < this.y + (this.height * 1.1)) {
            console.log("HIT!", x, y);
            game.score++;
        }
    },
    dot.updatePhysics = function (context) {
        // check if sprite has moved out of bounds
        if (this.x > context.canvas.width - this.width || this.x < 0) {
            this.vx = -this.vx;
        }
        this.x += this.vx;
        // check if sprite has moved out of bounds
        if (this.y > context.canvas.height - this.height || this.y < 0) {
            this.vy = -this.vy;
        }
        this.y += this.vy;
    }

    dot.refresh = function (context) {
        if (this.visible) {
        var whalf = Math.round(this.width / 2);
        var hhalf = Math.round(this.height / 2);
        var gradient = context.createRadialGradient(this.x, this.y, this.width*((Math.random()*0.2)+0.8), this.x, this.y, 0);
        gradient.addColorStop(0.1, "RGBA(0,0,0,0.00)");
        gradient.addColorStop(0.15, "RGBA(0,0,0,0.05)");
        gradient.addColorStop(0.8, "RGBA(" + this.color.r + ", " + this.color.g + ", " + this.color.b + ", 0.70)");
        context.fillStyle = gradient;
        context.fillRect(0, 0, this.x+this.width, this.y+this.height);
        }
        if (this.randomize) {
            this.randomDirection(context);
        }
    }

    return dot;
}