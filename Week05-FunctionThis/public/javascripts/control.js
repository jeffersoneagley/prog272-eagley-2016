function getNine() {
    return 9;
}

function getThis() {
    return this;
}

function getThisAnonymous() {
    return window;
}

var myObject = {
    getThis: function() {
        return this;
    }
};

var myFunction = {
    getThis: function() {
        return this;
    }
};

function MyFunction() {
    this.getThis = function() {
        return this;
    };
    return this;
}

function getThisStrict() {
    'use strict';
    return this;
}

$(document).ready(function() {
    'use strict';
});
